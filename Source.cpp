#include <iostream>
#include <string>
#include <string_view>
char q = '\n';

//��������� �������� �������� ����, ���������� �� ������ � 
//������� ��� �������
template <typename PrefixType>
void test_prefix_print(std::string& str, PrefixType prefix)
{
	auto helloWorld = std::string("HelloWorld");
	std::string str1;
	if (helloWorld.front() == prefix)
	{
		str1 = "True";
	}
	else
	{
		(str1 = "False");
	}
	std::cout << '\'' << str << "'starts with'" << prefix << "':" << str1 << q;

}


int main()
{
	std::string First ("Learnin_Strings");
	std::string Second;
	std::string Third("Strings_Learnin");
	Second = "Testing different options of strings";
	std::cout << First << '\n';



	//������� ������ ������
	std::cout << "Size: " <<First.size() << q;

	//������� ����������� ������
	std::cout << "capacity: " << First.capacity() << q;

	//������� ������ ������ ������
	std::cout << "First: " << First.front() << q;

	// char& f ��������� �������� ������ ������ � First �� �������� ���������� f,
	// ��� $, ������� �� ��������, L ������� �������
	char& f = First.front();
	f = 'l';
	std::cout << "Changing First with char$ f = 'l': " << First << q;

	// ������� ��������� ������ ������
	std::cout << "Last (back): " << First.back() << q;

	//������� ���������� ���� char - back � ���������� � �� ��������� ������ � First,
	//����� ������� �������� back
	char back = First.back();
	std::cout <<  "back = First.back(): " << back << q;



	//�������� ������ �� ������ ������� �� �
	First.at(2) = 'x';
	std::cout << First << '\n';

	/*i ��������� �������� ������ First = 15 -1 = 14
	� ��������� � ������� (������ ������� s? s - 15� ������)
	����� ������� �� 2, 14/2 = 7, �� ��������� _ (8� ������)
	������� �� 2, 7/2=4 (������ ���������) ������� r(4� ������)
	������� �� 2, ������� e(2� ������)*/

	for (unsigned i = First.length() - 1; i != 0; i /= 2) 
		std::cout << First[i] << q;

	for (unsigned i = First.length() - 1; i != 0; i /= 3) // 15�, 5�, 2�
		std::cout << First[i] << q;

	//������ ��������� ������ �� D
	First[First.size() - 1] = 'D';
	std::cout << First << q;



	// ��������� ����� ����� ���������� �������
	First.insert(15, " And how to deal with them");
	std::cout << First << q;
	
	// ��������� g ����� learnin, ��������� ������ ����� ��������� 7, � �� �� ������ ����� 8
	First.insert(7, "g");
	std::cout << First << q;

	// ��������� ! ����� ���������� �������
	First.push_back('!');
	std::cout << First << q << First.length() << q;

	// ������� ��������� ������
	First.pop_back();
	std::cout << First << q << First.length() << q;

	// ��������� � ������ First ������ Second, �� ������������?
	First.append(Second);
	std::cout << First << q;

	//���������� ��� ������ � ������� �������� 1,0,-1
	First = ("Strings_Learnin");
	int compare_value {First.compare(Third)};
	std::cout << compare_value << q;

	First = ("Learnin_Strings");
	int compare_value2{ First.compare(Third) };
	
	std::cout << compare_value2 << q;

	First = ("Learnin_String_and_over_stuff");
	int compare_value3{ First.compare(1,1,Third) };

	std::cout << compare_value3 << q;


	//������� ������ �������� true/false � ����������� �� ����
	//������������ �� ������ ������� � ������, ������ ���, ��� 
	//�� �������� .starts_with �������� ��������� ������ ������ 
	//��������
	//������ ����� ������ boolaalpha �� �����
	//�� ���� ��������� auto ��� ��������� ���������� ��� ����������
	std::boolalpha(std::cout);
	auto helloWorld = std::string("HelloWorld");
	test_prefix_print(helloWorld, 'H');


	std::cout << Second;


}